package ru.nsu.fit.endpoint.service.apitests;


import org.junit.Assert;
import org.slf4j.Logger;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.nsu.fit.endpoint.service.database.DBService;
import ru.nsu.fit.endpoint.service.database.data.Customer;
import ru.nsu.fit.endpoint.service.database.data.TopUpData;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.UUID;

public class Scenery_2 {

    private DBService dbService;
    private Logger logger;
    private UUID customerID;
    private Client client = ClientBuilder.newClient();
    private String REST_URI = "http://localhost:8080/endpoint/rest";


    @BeforeClass
    public void create() throws Exception {




        Customer customer = new Customer()
                .setId(null)
                .setFirstName("John")
                .setLastName("Wick")
                .setLogin("john_wick2@gmail.com")
                .setPass("Baba_Jaga")
                .setBalance(0);

        Response customerResponse = client
                .target(REST_URI + "/create_customer")
                .request(MediaType.APPLICATION_JSON)
                .header("Authorization","Basic YWRtaW46c2V0dXA=")
                .post(Entity.entity(customer, MediaType.APPLICATION_JSON));

        customer = customerResponse.readEntity(Customer.class);

        customerID = customer.getId();



    }

    @Test
    public void testTopUpBalance() throws Exception {

        TopUpData topUpData = new TopUpData();
        topUpData.setId(customerID);
        topUpData.setAmount(100);


        client
                .target(REST_URI + "/top_up_balance")
                .request(MediaType.APPLICATION_JSON)
                .header("Authorization","Basic YWRtaW46c2V0dXA=")
                .post(Entity.entity(topUpData, MediaType.APPLICATION_JSON));

        Response customerResponse = client
                .target(REST_URI + "/get_customer_id/john_wick2@gmail.com")
                .request(MediaType.APPLICATION_JSON)
                .header("Authorization","Basic YWRtaW46c2V0dXA=")
                .get();


        Customer customer = customerResponse.readEntity(Customer.class);                                                        customer.setBalance(100);
        Assert.assertEquals(customer.getBalance(), 100);


    }
}

