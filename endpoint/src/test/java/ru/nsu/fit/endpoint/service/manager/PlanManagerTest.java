package ru.nsu.fit.endpoint.service.manager;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.slf4j.Logger;
import ru.nsu.fit.endpoint.service.database.DBService;
import ru.nsu.fit.endpoint.service.database.data.Plan;

import java.util.UUID;

public class PlanManagerTest {
    private DBService dbService;
    private Logger logger;
    private PlanManager planManager;

    private Plan planBeforeCreateMethod;
    private Plan planAfterCreateMethod;

    @Before
    public void before() {
        // create stubs for the test's class
        dbService = Mockito.mock(DBService.class);
        logger = Mockito.mock(Logger.class);

        planBeforeCreateMethod = new Plan()
                .setId(null)
                .setName("Hello World")
                .setDetails("ok")
                .setFee(1);


        planAfterCreateMethod = planBeforeCreateMethod.clone();

        planAfterCreateMethod.setId(UUID.randomUUID());

        Mockito.when(dbService.createPlan(planBeforeCreateMethod)).thenReturn(planAfterCreateMethod);

        // create the test's class
        planManager = new PlanManager(dbService, logger);
    }

    @Test
    public void testCreateNewPlan() {
        // Вызываем метод, который хотим протестировать
        Plan plan = planManager.createPlan(planBeforeCreateMethod);

        // Проверяем результат выполенния метода
        Assert.assertEquals(plan.getId(), planAfterCreateMethod.getId());

        // Проверяем, что метод мока базы данных был вызван 1 раз
        Assert.assertEquals(1, Mockito.mockingDetails(dbService).getInvocations().size());
    }

    @Test
    public void testCreatePlanWithNullArgument() {
        try {
            planManager.createPlan(null);
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("Argument 'plan' is null", ex.getMessage());
        }
    }

    @Test
    public void testCreatePlanWithShortName() {
        try {
            planBeforeCreateMethod.setName("a");
            planManager.createPlan(planBeforeCreateMethod);
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("Name's length should be 2<=length<=128", ex.getMessage());
        }
    }
    @Test
    public void testCreatePlanWithLongName() {
        try {
            String s = "";
            for (int i = 0; i < 129; i++){
                s = s.concat("A");
            }
            planBeforeCreateMethod.setName(s);
            planManager.createPlan(planBeforeCreateMethod);

        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("Name's length should be 2<=length<=128", ex.getMessage());
        }
    }
    @Test
    public void testCreatePlanWithShortDetails() {
        try {
            planBeforeCreateMethod.setDetails("");
            planManager.createPlan(planBeforeCreateMethod);
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("Detail's length should be 1<=length<=1024", ex.getMessage());
        }
    }
    @Test
    public void testCreatePlanWithLongDetails() {
        try {
            String s = "";
            for (int i = 0; i < 1025; i++){
                s = s.concat("A");
            }
            planBeforeCreateMethod.setDetails(s);
            planManager.createPlan(planBeforeCreateMethod);
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("Detail's length should be 1<=length<=1024", ex.getMessage());
        }
    }

    @Test
    public void testRemovePlan() {
        planManager.removePlan(UUID.randomUUID());
    }



}
