package ru.nsu.fit.endpoint.service.apitests;


import org.junit.Assert;
import org.slf4j.Logger;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.nsu.fit.endpoint.service.database.DBService;
import ru.nsu.fit.endpoint.service.database.data.Customer;
import ru.nsu.fit.endpoint.service.database.data.Plan;
import ru.nsu.fit.endpoint.service.database.data.TopUpData;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.UUID;

public class Scenery_4 {

    private DBService dbService;
    private Logger logger;
    private UUID customerID;
    private Client client = ClientBuilder.newClient();
    private String REST_URI = "http://localhost:8080/endpoint/rest";


    @BeforeClass
    public void create() throws Exception {




        Plan plan = new Plan()
                .setFee(100)
                .setDetails("Plan")
                .setName("Plan");


        Response planResponse = client
                .target(REST_URI + "/create_plan")
                .request(MediaType.APPLICATION_JSON)
                .header("Authorization","Basic YWRtaW46c2V0dXA=")
                .post(Entity.entity(plan, MediaType.APPLICATION_JSON));



        Thread.sleep(1000);
    }

    @Test
    public void testCreatePlan() throws Exception {
        Response planResponse = client
                .target(REST_URI + "/get_plans")
                .request(MediaType.APPLICATION_JSON)
                .header("Authorization","Basic YWRtaW46c2V0dXA=")
                .get();

    }
}

