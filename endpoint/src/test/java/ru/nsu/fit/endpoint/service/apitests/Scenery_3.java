package ru.nsu.fit.endpoint.service.apitests;


import org.junit.Assert;
import org.slf4j.Logger;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.nsu.fit.endpoint.service.database.DBService;
import ru.nsu.fit.endpoint.service.database.data.Customer;
import ru.nsu.fit.endpoint.service.database.data.TopUpData;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.UUID;

public class Scenery_3 {

    private DBService dbService;
    private Logger logger;
    private UUID customerID;
    private Client client = ClientBuilder.newClient();
    private String REST_URI = "http://localhost:8080/endpoint/rest";


    @BeforeClass
    public void create() throws Exception {




        Customer customer = new Customer()
                .setId(null)
                .setFirstName("John")
                .setLastName("Wick")
                .setLogin("john_wick3@gmail.com")
                .setPass("Baba_Jaga")
                .setBalance(0);

        Response customerResponse = client
                .target(REST_URI + "/create_customer")
                .request(MediaType.APPLICATION_JSON)
                .header("Authorization","Basic YWRtaW46c2V0dXA=")
                .post(Entity.entity(customer, MediaType.APPLICATION_JSON));

        customer = customerResponse.readEntity(Customer.class);
        customerID = customer.getId();


        Customer customer_updated = customer
                .setFirstName("Wick")
                .setLastName("John")
                .setId(customerID);

        client
                .target(REST_URI + "/update_customer")
                .request(MediaType.APPLICATION_JSON)
                .header("Authorization","Basic YWRtaW46c2V0dXA=")
                .post(Entity.entity(customer_updated, MediaType.APPLICATION_JSON));
        Thread.sleep(1000);
    }

    @Test
    public void testUpdateCustomer() throws Exception {

        TopUpData topUpData = new TopUpData();
        topUpData.setId(customerID);
        topUpData.setAmount(100);

        Response customerResponse = client
                .target(REST_URI + "/get_customer_id/john_wick3@gmail.com")
                .request(MediaType.APPLICATION_JSON)
                .header("Authorization","Basic YWRtaW46c2V0dXA=")
                .get();


        Customer customer = customerResponse.readEntity(Customer.class);

        Assert.assertEquals("Wick", customer.getFirstName());
        Assert.assertEquals("John", customer.getLastName());



    }
}

