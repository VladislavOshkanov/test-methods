package ru.nsu.fit.endpoint.rest;

import org.apache.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.ext.Provider;
import java.io.IOException;




@Logged
@Provider
public class RequestLoggingFilter implements ContainerRequestFilter {

    private static Logger logger = org.apache.log4j.LogManager.getRootLogger();

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        logger.info(requestContext.getRequest());


    }
}