$(document).ready(function(){
    $("#create_plan_id").click(
        function() {
            var pName = $("#plan_name_id").val();
            var pFee = $("#plan_fee_id").val();
            var pDetails = $("#plan_details_id").val();

            // check fields
            if(pName =='' || pFee =='' || pDetails == '') {
                $('input[type="text"]').css("border","2px solid red");
                $('input[type="text"]').css("box-shadow","0 0 3px red");
                alert("All fields required");
            } else {
                $.post({
                    url: 'rest/create_plan',
                    headers: {
                        'Authorization': 'Basic ' + btoa('admin' + ':' + 'setup'),
                        'Content-Type': 'application/json'
                    },
                    data: JSON.stringify({
                                         	"name":pName,
                                             "details":pDetails,
                                             "fee":pFee,
                                         })
                }).done(function(data) {
                     $.redirect('/endpoint/plans.html', {'login': 'admin', 'pass': 'setup', 'role': 'ADMIN'}, 'GET');
                });
            }
        }
    );
});