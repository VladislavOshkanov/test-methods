$(document).ready(function(){
    $("#top_up_id").click(
        function() {
            var email = $("#email_id").val();
            var amount = $("#amount_id").val();

            // check fields
            if(email =='' || amount =='') {
                $('input[type="text"],input[type="password"]').css("border","2px solid red");
                $('input[type="text"],input[type="password"]').css("box-shadow","0 0 3px red");
                alert("Email or password is empty");
            } else {
                $.post({
                    url: 'rest/topup_balance_by_email',
                    headers: {
                        'Authorization': 'Basic ' + btoa('admin' + ':' + 'setup'),
                        'Content-Type': 'application/json'
                    },
                    data: JSON.stringify({
                                             "email": email,
                                             "amount": amount,
                                         })
                }).done(function(data) {
                     $.redirect('/endpoint/customers.html', {'login': 'admin', 'pass': 'setup', 'role': 'ADMIN'}, 'GET');
                });
            }
        }
    );
});