Response: <!doctype html><html lang="en"><head><title>HTTP Status 500 – Internal Server Error</title><style type="text/css">h1 {font-family:Tahoma,Arial,sans-serif;color:white;background-color:#525D76;font-size:22px;} h2 {font-family:Tahoma,Arial,sans-serif;color:white;background-color:#525D76;font-size:16px;} h3 {font-family:Tahoma,Arial,sans-serif;color:white;background-color:#525D76;font-size:14px;} body {font-family:Tahoma,Arial,sans-serif;color:black;background-color:white;} b {font-family:Tahoma,Arial,sans-serif;color:white;background-color:#525D76;} p {font-family:Tahoma,Arial,sans-serif;background:white;color:black;font-size:12px;} a {color:black;} a.name {color:black;} .line {height:1px;background-color:#525D76;border:none;}</style></head><body><h1>HTTP Status 500 – Internal Server Error</h1><hr class="line" /><p><b>Type</b> Exception Report</p><p><b>Message</b> java.lang.RuntimeException: com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException: Unrecognized field &quot;money&quot; (class ru.nsu.fit.endpoint.service.database.data.Customer), not marked as ignorable (6 known properties: &quot;lastName&quot;, &quot;pass&quot;, &quot;login&quot;, &quot;id&quot;, &quot;firstName&quot;, &quot;balance&quot;])</p><p><b>Description</b> The server encountered an unexpected condition that prevented it from fulfilling the request.</p><p><b>Exception</b></p><pre>javax.servlet.ServletException: java.lang.RuntimeException: com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException: Unrecognized field &quot;money&quot; (class ru.nsu.fit.endpoint.service.database.data.Customer), not marked as ignorable (6 known properties: &quot;lastName&quot;, &quot;pass&quot;, &quot;login&quot;, &quot;id&quot;, &quot;firstName&quot;, &quot;balance&quot;])
 at [Source: {
	&quot;firstName&quot;:&quot;Johnds&quot;,
    &quot;lastName&quot;:&quot;Weak&quot;,
    &quot;login&quot;:&quot;helloworld123@login.com&quot;,
    &quot;pass&quot;:&quot;password123&quot;,
    &quot;money&quot;:&quot;100&quot;
}; line: 6, column: 14] (through reference chain: ru.nsu.fit.endpoint.service.database.data.Customer[&quot;money&quot;])
	org.glassfish.jersey.servlet.WebComponent.service(WebComponent.java:423)
	org.glassfish.jersey.servlet.ServletContainer.service(ServletContainer.java:386)
	org.glassfish.jersey.servlet.ServletContainer.service(ServletContainer.java:334)
	org.glassfish.jersey.servlet.ServletContainer.service(ServletContainer.java:221)
	org.apache.tomcat.websocket.server.WsFilter.doFilter(WsFilter.java:53)
</pre><p><b>Root Cause</b></p><pre>java.lang.RuntimeException: com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException: Unrecognized field &quot;money&quot; (class ru.nsu.fit.endpoint.service.database.data.Customer), not marked as ignorable (6 known properties: &quot;lastName&quot;, &quot;pass&quot;, &quot;login&quot;, &quot;id&quot;, &quot;firstName&quot;, &quot;balance&quot;])
 at [Source: {
	&quot;firstName&quot;:&quot;Johnds&quot;,
    &quot;lastName&quot;:&quot;Weak&quot;,
    &quot;login&quot;:&quot;helloworld123@login.com&quot;,
    &quot;pass&quot;:&quot;password123&quot;,
    &quot;money&quot;:&quot;100&quot;
}; line: 6, column: 14] (through reference chain: ru.nsu.fit.endpoint.service.database.data.Customer[&quot;money&quot;])
	ru.nsu.fit.endpoint.shared.JsonMapper.fromJson(JsonMapper.java:23)
	ru.nsu.fit.endpoint.rest.RestService.createCustomer(RestService.java:62)
	sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
	sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	java.lang.reflect.Method.invoke(Method.java:498)
	org.glassfish.jersey.server.model.internal.ResourceMethodInvocationHandlerFactory$1.invoke(ResourceMethodInvocationHandlerFactory.java:81)
	org.glassfish.jersey.server.model.internal.AbstractJavaResourceMethodDispatcher$1.run(AbstractJavaResourceMethodDispatcher.java:144)
	org.glassfish.jersey.server.model.internal.AbstractJavaResourceMethodDispatcher.invoke(AbstractJavaResourceMethodDispatcher.java:161)
	org.glassfish.jersey.server.model.internal.JavaResourceMethodDispatcherProvider$ResponseOutInvoker.doDispatch(JavaResourceMethodDispatcherProvider.java:160)
	org.glassfish.jersey.server.model.internal.AbstractJavaResourceMethodDispatcher.dispatch(AbstractJavaResourceMethodDispatcher.java:99)
	org.glassfish.jersey.server.model.ResourceMethodInvoker.invoke(ResourceMethodInvoker.java:389)
	org.glassfish.jersey.server.model.ResourceMethodInvoker.apply(ResourceMethodInvoker.java:347)
	org.glassfish.jersey.server.model.ResourceMethodInvoker.apply(ResourceMethodInvoker.java:102)
	org.glassfish.jersey.server.ServerRuntime$2.run(ServerRuntime.java:308)
	org.glassfish.jersey.internal.Errors$1.call(Errors.java:271)
	org.glassfish.jersey.internal.Errors$1.call(Errors.java:267)
	org.glassfish.jersey.internal.Errors.process(Errors.java:315)
	org.glassfish.jersey.internal.Errors.process(Errors.java:297)
	org.glassfish.jersey.internal.Errors.process(Errors.java:267)
	org.glassfish.jersey.process.internal.RequestScope.runInScope(RequestScope.java:317)
	org.glassfish.jersey.server.ServerRuntime.process(ServerRuntime.java:291)
	org.glassfish.jersey.server.ApplicationHandler.handle(ApplicationHandler.java:1140)
	org.glassfish.jersey.servlet.WebComponent.service(WebComponent.java:403)
	org.glassfish.jersey.servlet.ServletContainer.service(ServletContainer.java:386)
	org.glassfish.jersey.servlet.ServletContainer.service(ServletContainer.java:334)
	org.glassfish.jersey.servlet.ServletContainer.service(ServletContainer.java:221)
	org.apache.tomcat.websocket.server.WsFilter.doFilter(WsFilter.java:53)
</pre><p><b>Root Cause</b></p><pre>com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException: Unrecognized field &quot;money&quot; (class ru.nsu.fit.endpoint.service.database.data.Customer), not marked as ignorable (6 known properties: &quot;lastName&quot;, &quot;pass&quot;, &quot;login&quot;, &quot;id&quot;, &quot;firstName&quot;, &quot;balance&quot;])
 at [Source: {
	&quot;firstName&quot;:&quot;Johnds&quot;,
    &quot;lastName&quot;:&quot;Weak&quot;,
    &quot;login&quot;:&quot;helloworld123@login.com&quot;,
    &quot;pass&quot;:&quot;password123&quot;,
    &quot;money&quot;:&quot;100&quot;
}; line: 6, column: 14] (through reference chain: ru.nsu.fit.endpoint.service.database.data.Customer[&quot;money&quot;])
	com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException.from(UnrecognizedPropertyException.java:51)
	com.fasterxml.jackson.databind.DeserializationContext.reportUnknownProperty(DeserializationContext.java:817)
	com.fasterxml.jackson.databind.deser.std.StdDeserializer.handleUnknownProperty(StdDeserializer.java:954)
	com.fasterxml.jackson.databind.deser.BeanDeserializerBase.handleUnknownProperty(BeanDeserializerBase.java:1324)
	com.fasterxml.jackson.databind.deser.BeanDeserializerBase.handleUnknownVanilla(BeanDeserializerBase.java:1302)
	com.fasterxml.jackson.databind.deser.BeanDeserializer.vanillaDeserialize(BeanDeserializer.java:249)
	com.fasterxml.jackson.databind.deser.BeanDeserializer.deserialize(BeanDeserializer.java:136)
	com.fasterxml.jackson.databind.ObjectMapper._readMapAndClose(ObjectMapper.java:3562)
	com.fasterxml.jackson.databind.ObjectMapper.readValue(ObjectMapper.java:2578)
	ru.nsu.fit.endpoint.shared.JsonMapper.fromJson(JsonMapper.java:21)
	ru.nsu.fit.endpoint.rest.RestService.createCustomer(RestService.java:62)
	sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
	sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	java.lang.reflect.Method.invoke(Method.java:498)
	org.glassfish.jersey.server.model.internal.ResourceMethodInvocationHandlerFactory$1.invoke(ResourceMethodInvocationHandlerFactory.java:81)
	org.glassfish.jersey.server.model.internal.AbstractJavaResourceMethodDispatcher$1.run(AbstractJavaResourceMethodDispatcher.java:144)
	org.glassfish.jersey.server.model.internal.AbstractJavaResourceMethodDispatcher.invoke(AbstractJavaResourceMethodDispatcher.java:161)
	org.glassfish.jersey.server.model.internal.JavaResourceMethodDispatcherProvider$ResponseOutInvoker.doDispatch(JavaResourceMethodDispatcherProvider.java:160)
	org.glassfish.jersey.server.model.internal.AbstractJavaResourceMethodDispatcher.dispatch(AbstractJavaResourceMethodDispatcher.java:99)
	org.glassfish.jersey.server.model.ResourceMethodInvoker.invoke(ResourceMethodInvoker.java:389)
	org.glassfish.jersey.server.model.ResourceMethodInvoker.apply(ResourceMethodInvoker.java:347)
	org.glassfish.jersey.server.model.ResourceMethodInvoker.apply(ResourceMethodInvoker.java:102)
	org.glassfish.jersey.server.ServerRuntime$2.run(ServerRuntime.java:308)
	org.glassfish.jersey.internal.Errors$1.call(Errors.java:271)
	org.glassfish.jersey.internal.Errors$1.call(Errors.java:267)
	org.glassfish.jersey.internal.Errors.process(Errors.java:315)
	org.glassfish.jersey.internal.Errors.process(Errors.java:297)
	org.glassfish.jersey.internal.Errors.process(Errors.java:267)
	org.glassfish.jersey.process.internal.RequestScope.runInScope(RequestScope.java:317)
	org.glassfish.jersey.server.ServerRuntime.process(ServerRuntime.java:291)
	org.glassfish.jersey.server.ApplicationHandler.handle(ApplicationHandler.java:1140)
	org.glassfish.jersey.servlet.WebComponent.service(WebComponent.java:403)
	org.glassfish.jersey.servlet.ServletContainer.service(ServletContainer.java:386)
	org.glassfish.jersey.servlet.ServletContainer.service(ServletContainer.java:334)
	org.glassfish.jersey.servlet.ServletContainer.service(ServletContainer.java:221)
	org.apache.tomcat.websocket.server.WsFilter.doFilter(WsFilter.java:53)
</pre><p><b>Note</b> The full stack trace of the root cause is available in the server logs.</p><hr class="line" /><h3>Apache Tomcat/9.0.2</h3></body></html>