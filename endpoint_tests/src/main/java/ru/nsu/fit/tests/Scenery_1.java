package ru.nsu.fit.tests;



import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


import org.slf4j.Logger;
import ru.nsu.fit.service.database.data.Customer;
import ru.yandex.qatools.allure.annotations.Title;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.UUID;

@Title("Create Customer test")
public class Scenery_1 {

    final static Logger logger = LoggerFactory.getLogger(Scenery_1.class);

    private UUID customerID;
    private Client client = ClientBuilder.newClient();
    private String REST_URI = "http://localhost:8080/endpoint/rest";
    private String email;

    @BeforeClass
    public void create() throws Exception {

        String unique = UUID.randomUUID().toString().substring(0, 7);
        email = unique + "@gmail.com";

        Customer customer = new Customer()
                .setId(null)
                .setFirstName("John")
                .setLastName("Wick")
                .setLogin(email)
                .setPass("Baba_Jaga")
                .setBalance(0);

        Response customerResponse = client
                .target(REST_URI + "/create_customer")
                .request(MediaType.APPLICATION_JSON)
                .header("Authorization","Basic YWRtaW46c2V0dXA=")
                .post(Entity.entity(customer, MediaType.APPLICATION_JSON));

        logger.info("Request to " + REST_URI + "/create_customer with parameters " + customer.toString()+ ". Response is " + customerResponse.toString() );
        customer = customerResponse.readEntity(Customer.class);

        customerID = customer.getId();

    }

    @Test
    public void testCreate() throws Exception {
        Response customerResponse = client
                .target(REST_URI + "/get_customer_id/" + email)
                .request(MediaType.APPLICATION_JSON)
                .header("Authorization","Basic YWRtaW46c2V0dXA=")
                .get();
        Customer customer = customerResponse.readEntity(Customer.class);
        Assert.assertEquals(customer.getId(), customerID);

    }
}

