package ru.nsu.fit.tests;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.nsu.fit.service.database.data.Plan;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.UUID;

public class Scenery_4 {

    final static Logger logger = LoggerFactory.getLogger(Scenery_1.class);
    private UUID customerID;
    private Client client = ClientBuilder.newClient();
    private String REST_URI = "http://localhost:8080/endpoint/rest";
    private String email;

    @BeforeClass
    public void create() throws Exception {



        Plan plan = new Plan()
                .setFee(100)
                .setDetails("Plan")
                .setName("Plan");


        Response planResponse = client
                .target(REST_URI + "/create_plan")
                .request(MediaType.APPLICATION_JSON)
                .header("Authorization","Basic YWRtaW46c2V0dXA=")
                .post(Entity.entity(plan, MediaType.APPLICATION_JSON));

        logger.info("Request to " + REST_URI + "/create_plan with parameters " + plan.toString()+ ". Response is " + planResponse.toString() );


        Thread.sleep(1000);
    }

    @Test
    public void testCreatePlan() throws Exception {
        Response planResponse = client
                .target(REST_URI + "/get_plans")
                .request(MediaType.APPLICATION_JSON)
                .header("Authorization","Basic YWRtaW46c2V0dXA=")
                .get();

    }
}

