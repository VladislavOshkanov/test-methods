package ru.nsu.fit.tests;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.apache.commons.io.FileUtils;

public class Test_gui_1 {

    public String email;

    @Test
    public void testCreateCustomer() throws Exception {


        String unique = UUID.randomUUID().toString().substring(0, 7);
        email = unique + "@gmail.com";

        driver.findElement(By.id("first_name_id")).sendKeys("John");
        driver.findElement(By.id("last_name_id")).sendKeys("Wick");
        driver.findElement(By.id("email_id")).sendKeys(email);
        driver.findElement(By.id("password_id")).sendKeys(unique + "J123");
        try {
            takeScreenshot();
        } catch (Exception e) {
            e.printStackTrace();
        }
        driver.findElement(By.id("create_customer_id")).click();
        driver.findElement(By.tagName("input")).sendKeys(email);
        takeScreenshot();
    }

    @BeforeMethod

    public void beforeMethod() {

        // Create a new instance of the Firefox driver

        driver = new FirefoxDriver();

        //Put a Implicit wait, this means that any search for elements on the page could take the time the implicit wait is set for before throwing exception

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        //Launch the Online Store Website

        driver.get("http://localhost:8080/endpoint/add_customer.html");

    }

    @AfterMethod

    public void afterMethod() {

        // Close the driver

        driver.quit();

    }
    public static void takeScreenshot() throws Exception {
        String timeStamp;
        File screenShotName;
        File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
//The below method will save the screen shot in d drive with name "screenshot.png"
        timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss.SSS").format(Calendar.getInstance().getTime());
        screenShotName = new File("/tmp/"+timeStamp+".png");
        FileUtils.copyFile(scrFile, screenShotName);

        String filePath = screenShotName.toString();
        String path = "<img src=\"file://" + filePath + "\" style=\"width:600px\"/>";
        Reporter.log(path);

    }

    private static WebDriver driver;
}
