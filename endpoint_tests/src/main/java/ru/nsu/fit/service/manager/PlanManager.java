package ru.nsu.fit.service.manager;

import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import ru.nsu.fit.service.database.DBService;
import ru.nsu.fit.service.database.data.Plan;

import java.util.List;
import java.util.UUID;

public class PlanManager extends ParentManager {
    public PlanManager(DBService dbService, Logger flowLog) {
        super(dbService, flowLog);
    }

    /**
     * Метод создает новый объект типа Plan. Ограничения:
     * name - длина не больше 128 символов и не меньше 2 включительно не содержит спец символов. Имена не пересекаются друг с другом;
    /* details - длина не больше 1024 символов и не меньше 1 включительно;
    /* fee - больше либо равно 0 но меньше либо равно 999999.
     */
    public Plan createPlan(Plan plan) {
        Validate.notNull(plan, "Argument 'plan' is null");

        Validate.isTrue(plan.getName().length() >= 2 && plan.getName().length() <= 128, "Name's length should be 2<=length<=128");
        Validate.isTrue(plan.getDetails().length() >= 1 && plan.getDetails().length() <= 1024, "Detail's length should be 1<=length<=1024");
        Validate.isTrue(plan.getFee() >= 0 && plan.getFee() <= 999999, "Fee should be 0<=fee<=999999");

        return dbService.createPlan(plan);
    }

    public Plan updatePlan(Plan plan)
    {
        Validate.notNull(plan, "Argument 'plan' is null");

        Validate.isTrue(plan.getName().length() >= 2 && plan.getName().length() <= 128, "Name's length should be 2<=length<=128");
        Validate.isTrue(plan.getDetails().length() >= 1 && plan.getDetails().length() <= 1024, "Detail's length should be 1<=length<=1024");
        Validate.isTrue(plan.getFee() >= 0 && plan.getDetails().length() <= 999999, "Fee should be 0<=fee<=999999");

        return dbService.updatePlan(plan);
    }

    public void removePlan(UUID id) {
        dbService.deletePlan(id);
    }

    /**
     * Метод возвращает список планов доступных для покупки.
     */
    public List<Plan> getPlans() {
        return dbService.getPlans();
    }
}
